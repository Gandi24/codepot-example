from django.views.generic import TemplateView
from utils.models import Question
from django.shortcuts import redirect
from django.contrib import messages


class QuestionView(TemplateView):
	template_name = 'question.html'

	def get(self, request, *args, **kwargs):
		self.question = Question.get_question_class()()
		return super(QuestionView, self).get(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		if request.POST.get('question_name'):
			self.question = Question.get_question_class(name=request.POST['question_name'])()
			if request.POST.get('answer') in self.question.correct_answers:
				self.question.correct_answer = True
				messages.success(request, "Your answer was correct. Try this one.")
			else:
				messages.error(request, "Your answer was incorrect. Try again.")
				context = self.get_context_data(**kwargs)
				self.question.save()
				return self.render_to_response(context)
		self.question.save()
		return redirect('question')

	def get_context_data(self, *args, **kwargs):
		context = super(QuestionView, self).get_context_data(*args, **kwargs)
		if self.question:
			context["question"] = self.question.as_dict()
		return context
from django.conf.urls import include, url
from django.contrib import admin
from quiz.views import QuestionView

urlpatterns = [
    url(r'^$', QuestionView.as_view(), name='question'),
]

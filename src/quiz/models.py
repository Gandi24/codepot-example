from utils.models import Question


class QuestionAboutColor(Question):
    text = "What is your favourite color?"
    options = ['red', 'black', 'yellow']
    correct_answers = ['red', 'black', 'yellow']


class QuestionAboutSparrow(Question):
    text = "What is the air-speed velocity of an unladen swallow?"
    options = ['5 meters per second', '11 meters per second', '16 meters per second', '21 meters per second']
    correct_answers = ['11 meters per second']


from utils.models import Question


class QuestionAboutWorkshop(Question):
    text = "Did you enjoy the workshop?"
    options = ['Yes', 'No']
    correct_answers = ['Yes']

from utils.models import Question


class CoolQuestionAboutVagrant(Question):
    text = "How to start Vagrant?"
    options = ['Status', 'Up', 'Destroy']
    correct_answers = ['Up']

from utils.models import Question


class QuestionAboutWeather(Question):
    text = "Current weather?"
    options = ['Hot', 'Cold', 'Rainy']
    correct_answers = ['Hot']

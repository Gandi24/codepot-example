from utils.models import Question


class QuestionTestA(Question):
    text = "Test A?"
    options = ['A', 'B', 'C']
    correct_answers = ['A']

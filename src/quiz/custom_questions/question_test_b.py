from utils.models import Question


class QuestionTestB(Question):
    text = "Test B?"
    options = ['A', 'B', 'C']
    correct_answers = ['B']

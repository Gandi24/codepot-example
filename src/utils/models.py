from django.db import models
import random

class Question(models.Model):
	class Meta:
		abstract = True

	text = None
	options = []
	correct_answers = []

	answer_date = models.DateTimeField(auto_now_add=True)
	correct_answer = models.BooleanField(default=False)

	@classmethod
	def get_question_class(cls, name=None):
		if name:
			for subclass in cls.__subclasses__():
				if subclass.__name__ == name:
					return subclass
		questions = cls.__subclasses__()
		if questions:
			return random.choice(questions)
		return cls

	def as_dict(self):
		try:
			return {
				'name': self.__class__.__name__,
				'all_answers': self.__class__.objects.count(),
				'correct_answers': self.__class__.objects.filter(correct_answer=True).count(),
				'text': self.text,
				'options': self.options,
				'last_answered': self.__class__.objects.latest('answer_date') if self.__class__.objects.exists() else None
			}
		except AttributeError:
			return {}